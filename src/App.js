import "./App.css";
import Books from "./pages/Books";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Registration from "./pages/Registration";
import Navbar from "./pages/Navbar";
import Categories from "./pages/Categories";
import { BrowserRouter, Routes, Route } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route exact path="books" element={<Books />} />
          <Route exact path="categories" element={<Categories />} />
          <Route exact path="registration" element={<Registration />} />
          <Route exact path="login" element={<Login />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;

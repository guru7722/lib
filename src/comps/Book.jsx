import React from "react";
import "../styles/main.css";

function Book({ url, name }) {
  return (
    <div className="book">
      <img src={url} alt="book-cover" className="img-com" />
      <p>{name}</p>
    </div>
  );
}

export default Book;

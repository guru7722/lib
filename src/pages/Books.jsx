import React from "react";
import Book from "../comps/Book";
import books from "../script/books.js";
import "../styles/main.css";
function Books() {
  return (
    <div className="book-page">
      {books.map((val) => {
        return <Book url={val.img} name={val.name} />;
      })}
    </div>
  );
}

export default Books;

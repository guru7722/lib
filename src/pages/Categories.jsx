import React from "react";
import Categ from "../comps/Categ";
import "../styles/main.css";

function Categories() {
  const arr = [
    "Romance",
    "Horror",
    "Action",
    "Fiction",
    "Romance",
    "Horror",
    "Action",
    "Fiction",
  ];
  return (
    <div className="Catg-holder">
      <h1>Categories</h1>
      {arr.map((val) => {
        return <Categ name={val} />;
      })}
    </div>
  );
}

export default Categories;

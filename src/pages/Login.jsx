import React from "react";
import LogComp from "../comps/LogComp";
import "../styles/main.css";

function Login() {
  return (
    <div className="Login-page">
      <LogComp />
    </div>
  );
}

export default Login;

import React from "react";
import "../styles/main.css";
import { BiLibrary } from "react-icons/bi";
import { Link } from "react-router-dom";
function Navbar() {
  return (
    <div className="Nav">
      <div className="logo">
        <BiLibrary />
        <p>Lib</p>
      </div>
      <ul>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/books">Books</Link>
        </li>
        <li>
          <Link to="/categories">Categories</Link>
        </li>
        <li>
          <Link to="/registration">Registration</Link>
        </li>
        <li>
          <Link to="/login">login</Link>
        </li>
      </ul>
    </div>
  );
}

export default Navbar;
